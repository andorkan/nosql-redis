<?php
// Headers necesarios
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// Incluye archivos de conexion 
include_once '../config/conexion.php';

// Instanciamos conexion a redis
$redis = new Conexion();
$bd = $redis->Conectar();

// Obtener informacion enviada por POST
$data = json_decode(file_get_contents("php://input"));
  
// Hacer validacion de informacion pasada aca
// Nos aseguramos que la informacion enviada no este vacia

        try{
            if($bd->hgetall("usuario.$data->correo") != null){
            	echo json_encode($bd->hMGet("error.101",['descripcion']), JSON_PRETTY_PRINT);
            }else{
            	$bd->hset("usuario.".$data->correo, "correo", $data->correo);
            	$bd->hset("usuario.".$data->correo, "contra", $data->contra);
            	$bd->hset("usuario.".$data->correo, "nombre", $data->nombre);
            	$bd->hset("usuario.".$data->correo, "apellido", $data->apellido);
            }
        }catch(Exeption $e){
            echo $e;
        }

?>
