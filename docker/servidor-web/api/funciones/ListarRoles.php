<?php

// Headers necesarios
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// incluye archivos de conexion y de clase Rol
include_once '../config/conexion.php';
  
// Instanciamos conexion a redis
$redis = new Conexion();
$bd = $redis->Conectar();
 
//Obtiene un arreglo de los roles en el sistema
$roles = $bd->keys("rol.*");
  
// Definir el codigo de respuesta como 200 OK
http_response_code(200);
  
// Imprimir en formato json
echo json_encode($roles);

?>
