<?php

// Headers necesarios
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// incluye archivos de conexion 
include_once '../config/conexion.php';
  
// Instanciamos conexion a redis
$redis = new Conexion();
$bd = $redis->Conectar();
 
// Obtiene un arreglo de los errores en el sistema
$arregloErrores = $bd->keys("error.*");

// Genera un arreglo con todos los codigos y sus descripciones
$errores = array();
foreach($arregloErrores as $e){
	$errores[] = $bd->hgetall($e); 
}

echo json_encode($errores, JSON_PRETTY_PRINT);

?>
