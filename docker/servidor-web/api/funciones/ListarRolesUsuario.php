<?php

// Headers necesarios
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// incluye archivos de conexion y de clase Rol
include_once '../config/conexion.php';
  
// Instanciamos conexion a redis
$redis = new Conexion();
$bd = $redis->Conectar();
 
// Obtener informacion enviada por POST
$data = json_decode(file_get_contents("php://input"));

//Obtiene un arreglo de los roles del usuario pasado por parametro
$roles = $bd->sMembers("usuario.$data->correo:tags");

// Imprimir en formato json
echo json_encode($roles, JSON_PRETTY_PRINT);

?>
