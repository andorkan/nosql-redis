<?php

// Headers necesarios
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// incluye archivos de conexion 
include_once '../config/conexion.php';
  
// Instanciamos conexion a redis
$redis = new Conexion();
$bd = $redis->Conectar();
 
//Obtiene un arreglo de los usuario en el sistema
$usuarios = $bd->keys("usuario.*");
  
// show products data in json format
echo json_encode($usuarios, JSON_PRETTY_PRINT);

?>
