<?php
// Headers necesarios
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// Incluye archivos de conexion
include_once '../config/conexion.php';

// Instanciamos conexion a redis
$redis = new Conexion();
$bd = $redis->Conectar();

// Obtener informacion enviada por POST
$data = json_decode(file_get_contents("php://input"));
  
        try{
            if($bd->hgetall("usuario.$data->correo") != null){
                if(strcmp(implode("",$bd->hMGet("usuario.$data->correo",['contra'])),$data->contra)==0){
		    echo json_encode(array("respuesta" => "true"), JSON_PRETTY_PRINT); 
                }else{
		    echo json_encode(array("respuesta" => "false"), JSON_PRETTY_PRINT); 
                }
            }else{
		echo json_encode(array("respuesta" => "false"), JSON_PRETTY_PRINT); 
            }
        }catch(Exeption $e){
            echo $e;
        }

?>
