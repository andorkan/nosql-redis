<?php
class Conexion{
    public function Conectar(){
        //Conexion al servidor de Redis en localhost
	
	$this->redis = null;

        try{
            $this->redis = new Redis();
            $this->redis->connect('servidor-redis', 6379); 
            $this->redis->auth('democlasenosql');
        }catch(RedisException $exception){
            echo "Error de conexion: " . $exception->getMessage();
        }
  
        return $this->redis;
    }
}
?>
