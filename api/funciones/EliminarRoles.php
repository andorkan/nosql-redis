<?php
// Headers necesarios
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// Incluye archivos de conexion 
include_once '../config/conexion.php';

// Instanciamos conexion a redis
$redis = new Conexion();
$bd = $redis->Conectar();

// Obtener informacion enviada por POST
$data = json_decode(file_get_contents("php://input"));

try{
        if($bd->hgetall("usuario.$data->correo") != null){
                if(strcmp(implode("",$bd->hMGet("usuario.$data->correo",['contra'])),$data->contra)==0){
			
			// La contrasena es correcta
			$i = 0;
			// Recorremos los roles pasados por parametro
			while(null != ($data->roles[$i])){
				// Chequeamos si el rol ya pertenecia al usuario
				if($bd->sIsMember("usuario.$data->correo:tags",$data->roles[$i]) == 1){
					// El rol ya pertenecia
					$bd->sRem("usuario.".$data->correo.":tags" , $data->roles[$i]);
					$i++;	
				}else{
					// El usuario no tenia asignado ese error
					echo json_encode(str_replace("variable",$data->roles[$i],$bd->hMGet("error.103",['descripcion'])), JSON_PRETTY_PRINT);
					$i++;	
				}
			}
                }else{
                        echo json_encode($bd->hMGet("error.104",['descripcion']), JSON_PRETTY_PRINT);
                }
        }else{
                echo json_encode($bd->hMGet("error.102",['descripcion']), JSON_PRETTY_PRINT);
        }
}catch(Exeption $e){
        echo $e;
}

?>
